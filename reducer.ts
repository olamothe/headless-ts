
import { AppState } from "./store";
import { ActionTypes } from "./actions";

export const reducer = (state: AppState, action: any) => {
    switch (action.type) {
        case ActionTypes.ADD_NAMES:
            return {
                ...state,
                names: state.names.concat(action.names)
            }
        default:
            return state;
    }
}