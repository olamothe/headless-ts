export interface AppState {
    names: string[]
}

export const defaultState: AppState = {
    names: []
}