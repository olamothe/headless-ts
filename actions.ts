export enum ActionTypes {
    ADD_NAMES,
    FETCH_NAMES
}

export const AddNamesAction = (names: string[]) => ({
    type: ActionTypes.ADD_NAMES,
    names
})
