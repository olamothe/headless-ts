const path = require('path');

module.exports = {
  entry: path.resolve('./headless.ts'),
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/,
      },
    ],
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
  },
  output: {
    library: 'headless-ts',
    libraryTarget: 'umd',
    filename: 'headless.js',
    path: path.resolve(__dirname, 'dist'),
  },
};