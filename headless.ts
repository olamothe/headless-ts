import { createStore } from 'redux'
import { reducer } from './reducer';
import { AddNamesAction } from './actions';
import { defaultState } from './store';

export class API {
    public store: any;
    constructor() {
        this.store = createStore(reducer, defaultState)
    }

    public async fetchNames() {
        const res = await fetch("https://dart.dev/f/portmanteaux.json")
        const names = await res.json()
        this.store.dispatch(AddNamesAction(names))
    }
}